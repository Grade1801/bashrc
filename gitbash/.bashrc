#!/bin/bash

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# Git bashrc Setup
if [ -f ~/.gitmanager ]; then
    . ~/.gitmanager
fi

LS_COLORS="$LS_COLORS:di=01;32"

if [ "$color_prompt" = yes ]; then
        #      W auf B     rhel+u   
	PS1="\[${User_space}\] 󱄛 \u \[${User_arrow}\]\[${Bash_main}\] 󰷏 \W \[${Bash_arrow}\]\[${Git_status}\]\$(parse_git_status)\[${Git_arrow}\]\[${Branch_main}\]\$(parse_git_branch)\[${Branch_arrow}\]\[${Main_reset}\]\012 $ " 
    else
	PS1='\u@\h:\W:\$ '
fi

