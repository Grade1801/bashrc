#!/bin/bash

if [ -f ~/.gitmanager ] && [ $(diff ~/.bashrc .bashrc | wc -l) -eq 0 ]; then
	echo -e "\n=================================="
	echo      " Gitbash is already installed !!!"
	echo -e   "==================================\n"
else
	if [ $(uname -a | grep MINGW64 | wc -l) -eq 0 ]	&& [ $(fc-list | grep SauceCodeProNerdFontMono | wc -l) -gt 0 ]; then
		sudo yum install fontconfig -y
		mkdir -p ~/.fonts
		cp .fonts/* ~/.fonts
		fc-cache -v
	fi

	cp ~/.bashrc ~/.bashrc.backup_$(date '+%Y%m%d_%H_%M')
	cat .bashrc > ~/.bashrc

	cat .gitmanager > ~/.gitmanager

	cp ~/.bash_aliases ~/.bash_aliases.backup_$(date '+%Y%m%d_%H_%M')
	cat .bash_aliases > ~/.bash_aliases 

	cat .gitconfig > ~/.gitconfig

	echo ""
	echo "================================="
	echo " Please logout from Terminal and" 
	echo " reconnect for taking effect!"
	echo "================================="
	echo ""
fi

