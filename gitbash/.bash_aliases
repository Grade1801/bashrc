# bash aliases
alias ll="ls -al"


# Git Aliases
alias gl="git log --decorate --oneline --graph"
alias gls="git log --decorate --oneline --graph --stat"
alias gs="git status"
